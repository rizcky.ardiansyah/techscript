/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React, {useState, useCallback} from 'react';
import {
  ScrollView,
  StatusBar,
  StyleSheet,
  useColorScheme,
  View,
  FlatList,
  RefreshControl,
} from 'react-native';
import {
  SafeAreaView,
  Text,
  Button,
  BookingHistory,
  Image,
  Card,
  PostItem,
} from './app/components';
import * as Utils from '@utils';
import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import {BaseColor} from './app/config';
import RowsItem from './RowsItem';

const initialList = [
  {
    id: '1',
    image: require('./app/assets/images/trip-1.jpg'),
    title: 'Overseas Adventure Travel In Nepal',
    likes: 0,
  },
  {
    id: '2',
    image: require('./app/assets/images/trip-2.jpg'),
    title: '50 Really Good Place For This Summer',
    likes: 0,
  },
  {
    id: '3',
    image: require('./app/assets/images/trip-3.jpg'),
    title: 'Overseas Adventure Travel In Nepal',
    likes: 0,
  },
  {
    id: '4',
    image: require('./app/assets/images/trip-4.jpg'),
    title: '50 Really Good Place For This Summer',
    likes: 0,
  },
];

function App() {
  const isDarkMode = useColorScheme() === 'dark';
  const [data, setData] = useState(initialList);
  const [refreshing] = useState(false);

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };
  const imagePost = {
    imagePost: {width: '100%', height: 360},
  };

  const onLikeAll = () => {
    const myList = [...data];
    myList.map(idx => {
      idx.likes++;
    });
    setData(myList);
  };

  const onResetAll = () => {
    const myList = [...data];
    myList.map(idx => {
      idx.likes = 0;
    });
    setData(myList);
  };

  const onDislikeAll = () => {
    const myList = [...data];
    myList.map(idx => {
      if (idx.likes > 0) {
        idx.likes--;
      }
    });
    setData(myList);
  };

  const onLike = key => {
    const myList = [...data];
    const artwork = myList.find(a => a.id === key);
    artwork.likes++;
    setData(myList);
    // setDep(key);
  };

  const onDislike = key => {
    const myList = [...data];
    const artwork = myList.find(a => a.id === key);
    if (artwork.likes > 0) {
      artwork.likes--;
    }
    setData(myList);
  };

  const [dep, setDep] = useState('');

  const renderItem = ({item}) => {
    // console.log('renderItem', item.id);
    // setDep(item.id);
    // return <RenderItem item={item} />;
    return <RowsItem item={item} onLike={onLike} onDislike={onDislike} />;
  };

  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar
        barStyle={isDarkMode ? 'light-content' : 'dark-content'}
        backgroundColor={backgroundStyle.backgroundColor}
      />
      <View
        style={{
          paddingHorizontal: 20,
          paddingVertical: 20,
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <Button onPress={() => onLikeAll()}>Like All</Button>
        <Button onPress={() => onResetAll()}>Reset All</Button>
        <Button onPress={() => onDislikeAll()}>Dislike All</Button>
      </View>
      {/* <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={backgroundStyle}> */}
      <View
        style={{
          backgroundColor: isDarkMode ? Colors.black : Colors.white,
          marginBottom: 50,
        }}>
        <FlatList
          refreshControl={
            <RefreshControl
              colors={[BaseColor.primary]}
              tintColor={BaseColor.primary}
              refreshing={refreshing}
              // onRefresh={() => {}}
            />
          }
          data={data}
          keyExtractor={(item, index) => item.id}
          // renderItem={({item}) => renderItem(item)}
          renderItem={renderItem}
        />
      </View>
      {/* </ScrollView> */}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
