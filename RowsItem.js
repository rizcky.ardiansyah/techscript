import React, {memo} from 'react';
import {View} from 'react-native';
import {
  SafeAreaView,
  Text,
  Button,
  BookingHistory,
  Image,
  Card,
  PostItem,
} from './app/components';

const RowsItem = memo(
  ({item, onLike, onDislike}) => {
    console.log('item', item);
    // return <View />;
    return (
      <View style={{margin: 10}}>
        <Image style={{width: '100%', height: 360}} source={item?.image} />
        <View
          style={{
            marginTop: 10,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Button>{item.likes} Like</Button>
          <View style={{flexDirection: 'row'}}>
            <Button style={{marginRight: 5}} onPress={() => onLike(item.id)}>
              Like
            </Button>
            <Button onPress={() => onDislike(item?.id)}>Dislike</Button>
          </View>
        </View>
      </View>
    );
  },
  (prevProps, nextProps) => {
    return prevProps.item.likes === nextProps.item.likes;
  },
);

export default RowsItem;
