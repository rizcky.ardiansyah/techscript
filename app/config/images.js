/**
 * Images Defines
 * @author Passion UI <passionui.com>
 */

export const Images = {
  trip1: require('@assets/trip-1.jpg'),
  trip2: require('@assets/trip-2.jpg'),
  trip3: require('@assets/trip-3.jpg'),
  trip4: require('@assets/trip-4.jpg'),
  trip5: require('@assets/trip-5.jpg'),
  trip6: require('@assets/trip-6.jpg'),
  trip7: require('@assets/trip-7.jpg'),
  trip8: require('@assets/trip-8.jpg'),
  trip9: require('@assets/trip-9.jpg'),
};
